package tcstream

func isSpace(ch byte) bool {
	switch ch {
	case '\t', '\n', '\v', '\f', '\r', ' ', 0x85, 0xA0:
		return true
	}
	return false
}
func isLower(ch byte) bool {
	return ch >= 'a' && ch <= 'z'
}

func toTitle(ch byte) byte {
	return ch - 32 // 32 = 'a' - 'A'
}

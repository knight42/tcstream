package tcstream

import (
	"io"
)

type State int8

const (
	StateInitial = iota
	StateObjectBegin
	StateObjectKeyBegin
	StateObjectKeyRemain
	StateObjectKeyEnd
	StateObjectValueBegin
)

type JSONStream struct {
	r io.Reader

	state     State
	nestLevel int
}

var _ io.Reader = (*JSONStream)(nil)

func New(r io.Reader) io.Reader {
	return &JSONStream{
		r: r,
	}
}

func (s *JSONStream) decrLvlAndSetState() {
	s.nestLevel--
	if s.nestLevel == 0 {
		s.state = StateInitial
	} else {
		s.state = StateObjectBegin
	}
}

func (s *JSONStream) incrLvlAndSetState() {
	s.nestLevel++
	s.state = StateObjectBegin
}

func (s *JSONStream) Read(p []byte) (n int, err error) {
	n, err = s.r.Read(p)
	if err != nil {
		return n, err
	}

	data := p[:n]
	i := 0
	for i < n {
		ch := data[i]
		if isSpace(ch) {
			i++
			continue
		}

		switch s.state {
		case StateInitial:
			switch ch {
			case '{':
				s.incrLvlAndSetState()
			}
		case StateObjectBegin:
			switch ch {
			case '"':
				s.state = StateObjectKeyBegin
			case '}':
				s.decrLvlAndSetState()
			}
		case StateObjectKeyBegin:
			if isLower(ch) {
				data[i] = toTitle(ch)
			}
			s.state = StateObjectKeyRemain
		case StateObjectKeyRemain:
			switch ch {
			case '\\':
				i++
			case '"':
				s.state = StateObjectKeyEnd
			}
		case StateObjectKeyEnd:
			switch ch {
			case '{':
				s.incrLvlAndSetState()
			case '}':
				s.decrLvlAndSetState()
			case ':':
				s.state = StateObjectValueBegin
			}
		case StateObjectValueBegin:
			switch ch {
			case '\\':
				i++
			case ',':
				s.state = StateObjectBegin
			case '{':
				s.incrLvlAndSetState()
			case '}':
				s.decrLvlAndSetState()
			}
		}
		i++
	}

	return n, nil
}

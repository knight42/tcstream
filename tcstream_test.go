package tcstream

import (
	"bytes"
	"io"
	"strings"
	"testing"
)

func TestStream(t *testing.T) {
	s := `
{
    "metadata": {
        "total": 1,
        "itemsLength": 1
    },
    "items": [{
        "stage": "foo",
        "file": "foo.go",
        "creationTimestamp": "2020-11-30T13:48:34.395Z"
    }, {
        "stage": "bar",
        "file": "bar.go",
        "creationTimestamp": "2020-11-30T13:48:34.395Z"
	}]
}
`
	expected := `
{
    "Metadata": {
        "Total": 1,
        "ItemsLength": 1
    },
    "Items": [{
        "Stage": "foo",
        "File": "foo.go",
        "CreationTimestamp": "2020-11-30T13:48:34.395Z"
    }, {
        "Stage": "bar",
        "File": "bar.go",
        "CreationTimestamp": "2020-11-30T13:48:34.395Z"
	}]
}
`
	stream := New(strings.NewReader(s))
	var buf bytes.Buffer
	_, err := io.Copy(&buf, stream)
	if err != nil {
		t.Fatal(err)
	}

	got := buf.String()
	if expected != got {
		t.Fatalf("Wrong result\nExpected: %s\nGot: %s\n", expected, got)
	}
}
